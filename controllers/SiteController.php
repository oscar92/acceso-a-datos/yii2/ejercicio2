<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\puerto;
use app\models\etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    //DAO
    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) as numero_ciclistas from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(dorsal) as numero_ciclistas FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_ciclistas'],
        "titulo"=>"consulta 1 con DAO",
        "enunciado"=>" Número de ciclistas que hay",
        "sql"=>"SELECT count(dorsal) as numero_ciclistas FROM ciclista",
    ]);
    }
    
    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) as numero_dciclistas from ciclista where nomequipo="Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(dorsal) as numero_ciclistas FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_ciclistas'],
        "titulo"=>"consulta 2 con DAO",
        "enunciado"=>" Número de ciclistas que hay del equipo Banesto",
        "sql"=>'SELECT count(dorsal) as numero_ciclistas FROM ciclista WHERE nomequipo="Banesto"',
    ]);
    }
    
    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand('select AVG(edad) as media_edad from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as media_edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['media_edad'],
        "titulo"=>"consulta 3 con DAO",
        "enunciado"=>"Edad media de los ciclistas",
        "sql"=>'SELECT AVG(edad) as media_edad FROM ciclista',
    ]);
    }
    
    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand('select AVG(edad) as media_edad from ciclista where nomequipo="Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as media_edad FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['media_edad'],
        "titulo"=>"consulta 4 con DAO",
        "enunciado"=>"La edad media de los del equipo Banesto",
        "sql"=>'SSELECT AVG(edad) as media_edad FROM ciclista WHERE nomequipo="Banesto"',
    ]);
    }
    
    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand('select AVG(edad) as media_edad, nomequipo from ciclista group by nomequipo')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(edad) as media_edad, nomequipo FROM ciclista GROUP BY nomequipo',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['media_edad','nomequipo'],
        "titulo"=>"consulta 5 con DAO",
        "enunciado"=>"La edad media de los ciclistas por cada equipo",
        "sql"=>'SELECT AVG(edad) as media_edad, nomequipo FROM ciclista GROUP BY nomequipo',
    ]);
    }
    
    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) as numero_ciclistas, nomequipo from ciclista group by nomequipo')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(dorsal) as numero_ciclistas, nomequipo FROM ciclista GROUP BY nomequipo',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_ciclistas','nomequipo'],
        "titulo"=>"consulta 6 con DAO",
        "enunciado"=>"El número de ciclistas por equipo",
        "sql"=>'SELECT count(dorsal) as numero_ciclistas, nomequipo FROM ciclista GROUP BY nomequipo',
    ]);
    }
    
    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) as numero_puertos from puerto')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(nompuerto) as numero_puertos FROM puerto',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_puertos'],
        "titulo"=>"consulta 7 con DAO",
        "enunciado"=>"El número total de puertos",
        "sql"=>'SELECT count(nompuerto) as numero_puertos FROM puerto',
    ]);
    }
    
    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) as numero_puertos from puerto where altura>1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(nompuerto) as numero_puertos FROM puerto WHERE altura>1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_puertos'],
        "titulo"=>"consulta 8 con DAO",
        "enunciado"=>"El número total de puertos mayores de 1500",
        "sql"=>'SELECT count(nompuerto) as numero_puertos FROM puerto WHERE altura>1500',
    ]);
    }
    
    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand('select nomequipo, COUNT(dorsal) as numero_ciclistas from ciclista group by nomequipo having COUNT(dorsal)>4 order by COUNT(dorsal)')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal)',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['nomequipo','numero_ciclistas'],
        "titulo"=>"consulta 9 con DAO",
        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
        "sql"=>'SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal)',
    ]);
    }
    
    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista WHERE edad>=28 AND edad <=32 GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal) ASC')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista WHERE edad>=28 AND edad <=32 GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal) ASC',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['nomequipo','numero_ciclistas'],
        "titulo"=>"consulta 10 con DAO",
        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
        "sql"=>'SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista WHERE edad>=28 AND edad <=32 GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal) ASC',
    ]);
    }
    
    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_etapas','dorsal'],
        "titulo"=>"consulta 11 con DAO",
        "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
        "sql"=>'SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal',
    ]);
    }
    
    public function actionConsulta12(){
        
        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1 ORDER BY COUNT(numetapa) ASC')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1 ORDER BY COUNT(numetapa) ASC',
            
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
       "resultados"=>$dataProvider,
        "campos"=>['numero_etapas','dorsal'],
        "titulo"=>"consulta 12 con DAO",
        "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
        "sql"=>'SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1 ORDER BY COUNT(numetapa) ASC',
    ]);
    }
    
    //Active Record
    public function actionConsultala1(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(dorsal) as numero_ciclistas"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(dorsal) as numero_ciclistas FROM ciclista",
        ]);
    }
    
    public function actionConsultala2(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(dorsal) as numero_ciclistas")->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>'SELECT count(dorsal) as numero_ciclistas FROM ciclista WHERE nomequipo="Banesto"',
        ]);
    }
    
    public function actionConsultala3(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("AVG(edad) as media_edad"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>'SELECT AVG(edad) as media_edad FROM ciclista',
        ]);
    }
    
    public function actionConsultala4(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("AVG(edad) as media_edad")->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>'SELECT AVG(edad) as media_edad FROM ciclista WHERE nomequipo="Banesto"',
        ]);
    }
    
    public function actionConsultala5(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("AVG(edad) as media_edad, nomequipo")->groupBy("nomequipo"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media_edad','nomequipo'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>'SELECT AVG(edad), nomequipo FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
    public function actionConsultala6(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(dorsal) as numero_ciclistas, nomequipo")->groupBy("nomequipo"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_ciclistas','nomequipo'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>'SELECT count(dorsal) as numero_ciclistas, nomequipo FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
    public function actionConsultala7(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("count(nompuerto) as numero_puertos"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_puertos'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>'SELECT count(nompuerto) as numero_puertos FROM puerto',
        ]);
    }
    
     public function actionConsultala8(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("count(nompuerto) as numero_puertos")->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_puertos'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>'SELECT count(nompuerto) as numero_puertos FROM puerto WHERE altura>1500',
        ]);
    }
    
    public function actionConsultala9(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => ciclista::find()->select("nomequipo, COUNT(dorsal) as numero_ciclistas")->groupBy("nomequipo")->having("COUNT(dorsal)>4")->orderBy("COUNT(dorsal)"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_ciclistas'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>'SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal)',
        ]);
    }
    
    public function actionConsultala10(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => ciclista::find()->select("nomequipo, COUNT(dorsal) as numero_ciclistas")->where("edad>=28 AND edad <=32 GROUP BY nomequipo")->having("COUNT(dorsal)>4")->orderBy("COUNT(dorsal) ASC"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','numero_ciclistas'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>'SELECT nomequipo, COUNT(dorsal) as numero_ciclistas FROM ciclista WHERE edad>=28 AND edad <=32 GROUP BY nomequipo HAVING COUNT(dorsal)>4 ORDER BY COUNT(dorsal) ASC',
        ]);
    }
    
    public function actionConsultala11(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => etapa::find()->select("COUNT(numetapa) as numero_etapas, dorsal")->groupBy("dorsal"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_etapas','dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>'SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal',
        ]);
    }
    
    
    public function actionConsultala12(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => etapa::find()->select("COUNT(numetapa) as numero_etapas, dorsal")->groupBy("dorsal")->having("COUNT(numetapa)>1")->orderBy("COUNT(numetapa) ASC"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numero_etapas','dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>'SELECT COUNT(numetapa) as numero_etapas, dorsal FROM etapa GROUP BY dorsal HAVING COUNT(numetapa)>1 ORDER BY COUNT(numetapa) ASC',
        ]);
    }


}
