<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 2</h1>
        
    </div>

    <div class="body-content">

        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h2>Consulta 1</h2>

                <p>Número de ciclistas que hay</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala1'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
                
            </div>
            <div  class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h2>Consulta 2</h2>

                <p>Número de ciclistas que hay del equipo Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala2'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
                
            </div>
            <div  class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h2>Consulta 3</h2>
                
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala3'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 4</h2>

                <p>La edad media de los del equipo Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala4'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 5</h2>

                <p>La edad media de los ciclistas por cada equipo</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala5'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 6</h2>
                
                <p>El número de ciclistas por equipo</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala6'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 7</h2>

                <p>El número total de puertos</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala7'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 8</h2>

                <p>El número total de puertos mayores de 1500</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala8'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 9</h2>
                
                <p>Listar el nombre de los equipos que tengan más de<br>
                    4 ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala9'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="">
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 10</h2>

                <p> Listar el nombre de los equipos que tengan más de<br>
                    4 ciclistas cuya edad esté entre 28 y 32</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala10'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 11</h2>

                <p>Indícame el número de etapas que ha ganado cada<br>
                    uno de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala11'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 cod-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Consulta 12</h2>

                <p>Indícame el dorsal de los ciclistas que hayan<br>
                    ganado más de una etapa</p>
                <p>
                    <?= Html::a('Active Record', ['site/consultala12'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
